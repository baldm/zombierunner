package Models;

public class Player {
    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private String direction="";
    private boolean top;
    private boolean right;
    private boolean bottom;
    private boolean left;
    private String lastState="";

    public Player() {
    }

    // Konstruktoren
    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 35;
        this.w = 35;
    }

    // Methoden
    public void move (int dx, int dy) {
        if((((this.x + dx)>-1) && ((this.x + dx)<1001)) &&
                (((this.y + dy)>-1) && ((this.y + dy)<601))) {
            this.x += dx;
            this.y += dy;
        }
    }


    // Getter + Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setBottom(boolean bottom) {
        this.bottom = bottom;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isTop() {
        return top;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isBottom() {
        return bottom;
    }

    public boolean isLeft() {
        return left;
    }

    public String getLastState() {
        return lastState;
    }

    public void setLastState(String lastState) {
        this.lastState = lastState;
    }
}
