package Models;

import java.util.LinkedList;
import java.util.List;

public class Model {
    // FINALS
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    // Eigenschaften
    private List<Zombie> zombies = new LinkedList<>();
    private Player player;
    private List<Bullet> bullets = new LinkedList<>();
    private int bulletCount =3;
    private double score;
    private boolean dead;

    // Konstruktoren
    public Model() {
        this.zombies.add(new Zombie( 0.05f, 0.05f));
        this.player = new Player(500, 300);
    }

    // Methoden
    public  void update (long elapsedTime) {
        //Spawn Zombies
        if(elapsedTime%11==0 && (score/1000)<=10){
            this.zombies.add(new Zombie(0.045f,0.045f));
        }else if (elapsedTime%11==0 && (score/1000)>10){
            this.zombies.add(new Zombie(0.055f,0.055f));
            this.zombies.add(new Zombie(0.055f,0.055f));
        }
        //Collision detection between Zombies and Bullets
        zombieNearBullet(zombies, bullets);

        //Collision detection between Zombies
        zombieNearZombie(zombies);


        //Zombie moving to Player
        zombieMovingToPlayer(zombies);
    }
    public void timeInSecs(long elapsedTime) {
        if (!dead) {
            score += elapsedTime;
        }else {
            System.out.println("You Died");
            System.out.println("You survived: " + score/1000 + " seconds");
            System.exit(1);
        }
    }

    public synchronized void zombieMovingToPlayer(List<Zombie> zombies){
        double speedMultiplier = 1.5d;
        for (Zombie zom : zombies){
            if (((this.player.getX() - zom.getX()) >= 0) && ((this.player.getY() - zom.getY()) >= 0)) {
                zom.setZombieDirection("Right");
                zom.move(speedMultiplier * 1, speedMultiplier * 1);
            } else if (((this.player.getX() - zom.getX()) <= 0) && ((this.player.getY() - zom.getY()) <= 0)) {
                zom.setZombieDirection("Top");
                zom.move(speedMultiplier * -1, speedMultiplier * -1);
            } else if (((this.player.getX() - zom.getX()) <= 0) && ((this.player.getY() - zom.getY()) >= 0)) {
                zom.setZombieDirection("Left");
                zom.move(speedMultiplier * -1, speedMultiplier * 1);
            } else if (((this.player.getX() - zom.getX()) >= 0) && ((this.player.getY() - zom.getY()) <= 0)) {
                zom.setZombieDirection("Down");
                zom.move(speedMultiplier * 1, speedMultiplier * -1);
            }
            if(((Math.abs((this.player.getX() - zom.getX())) <= 2) && (Math.abs((this.player.getY() - zom.getY())) <= 2))){
                dead = true;
            }
        }
    }

    public synchronized void zombieNearZombie(List<Zombie> zombieList){
        for (int i = 0; i <= zombieList.size()-2; i++) {
            for (int j = i+1; j<= zombieList.size()-2; j++){
                if((Math.abs(zombieList.get(i).getX()-zombieList.get(j).getX())<27) &&
                        (Math.abs(zombieList.get(i).getY()-zombieList.get(j).getY())<27)){
                    zombieList.get(i).setColliding(true);
                }
            }
        }
    }

    public synchronized void zombieNearBullet(List<Zombie> zombieList, List<Bullet> bulletList){
        try {
            for (int i = 0; i <= zombieList.size() - 1; i++) {
                for (int j = i; j <= bulletList.size() - 1; j++) {
                    if ((Math.abs(zombieList.get(i).getX() - bulletList.get(j).getX()) < 30) &&
                            (Math.abs(zombieList.get(i).getY() - bulletList.get(j).getY()) < 30)) {
                        try {
                            for (int ii = 0; ii <= zombieList.size() - 1; ii++) {
                                for (int jj = ii; jj <= bulletList.size() - 1; jj++) {
                                    if (((Math.abs(zombieList.get(ii).getX() - bulletList.get(jj).getX())) <= 150) &&
                                    ((Math.abs(zombieList.get(ii).getY() - bulletList.get(jj).getY())) <= 150)) {
                                    zombieList.remove(ii);
                                    bulletList.remove(j);
                                    }
                                }
                            }
                        }
                        catch (Exception ee){}
                    }
                }
            }
        }
        catch(Exception e){};
    }


    // Setter + Getter
    public List<Zombie> getZombies() {
        return zombies;
    }

    public Player getPlayer() {
        return player;
    }

    public List<Bullet> getBullets() {
        return bullets;
    }

    public void addBullets(Bullet bullet) {
        this.bullets.add(bullet);
    }

    public double getScore() {
        return score;
    }


    public int getBulletCount() {
        return bulletCount;
    }

    public void setBulletCount(int bulletCount) {
        this.bulletCount = bulletCount;
    }

}
