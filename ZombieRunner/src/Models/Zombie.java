package Models;

public class Zombie {
    // Eigenschaften
    private Double x;
    private Double y;
    private boolean isColliding=false;
    private int h;
    private int w;
    private float speedX;
    private float speedY;
    private String zombieDirection ="";

    // Konstruktoren
    public Zombie(float speedX, float speedY) {
        randomSpawn();
        this.h = 30;
        this.w = 30;
        this.speedX = speedX;
        this.speedY = speedY;
    }

    // Methoden
    public void randomSpawn(){
        int side = (int)(Math.random()*4)+1;
        switch (side){
            case 1:{// spawn left
                this.x = (Math.random()*-1)+1;
                this.y = (Math.random()*600)+1;
                break;
            }
            case 2:{// spawn top
                this.x = (Math.random()*1000)+1;
                this.y = (Math.random()*-1)+1;
                break;
            }
            case 3:{// spawn right
                this.x = (Math.random()*1)+1000;
                this.y = (Math.random()*600)+1;
                break;
            }
            case 4:{// spawn bottom
                this.x = (Math.random()*1000)+1;
                this.y = (Math.random()*1)+600;
                break;
            }
        }
    }


    public void move (double dx, double dy) {
        if(!this.isColliding) {
            this.x += dx;
            this.y += dy;
        }
        else {
            this.x -= 0.5*dx;
            this.y -= 0.5*dy;
            this.isColliding=false;
        }
    }


    // Setter + Getter
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public int getH() {
        return this.h;
    }

    public int getW() {
        return this.w;
    }

    public float getSpeedX() {
        return this.speedX;
    }

    public float getSpeedY() {
        return this.speedY;
    }

    public boolean isColliding() {
        return isColliding;
    }

    public void setColliding(boolean colliding) {
        isColliding = colliding;
    }

    public String getZombieDirection() {
        return zombieDirection;
    }

    public void setZombieDirection(String zombieDirection) {
        this.zombieDirection = zombieDirection;
    }
}
