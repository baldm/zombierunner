import Models.Bullet;
import Models.Model;
import Models.Zombie;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc){
        this.model = model;
        this.gc = gc;
    }

    public void draw(){
        gc.clearRect(0,0,Model.WIDTH, Model.HEIGHT);

        //Player
        String lastState = model.getPlayer().getLastState();
        Image playerInitialImg = new Image("Sprites/playerRight.png");
        switch (lastState){
            case "top":{
                playerInitialImg = new Image("Sprites/playerTop.png");
                break;
            }
            case "right":{
                playerInitialImg = new Image("Sprites/playerRight.png");
                break;
            }
            case "left":{
                playerInitialImg = new Image("Sprites/playerLeft.png");
                break;
            }
            case "bottom":{
                playerInitialImg = new Image("Sprites/playerDown.png");
                break;
            }
        }
        Image playerImg = playerInitialImg;
        String playerDirection = model.getPlayer().getDirection();
        switch (playerDirection){
            case "Top":{
                playerImg = new Image("Sprites/playerTop.png");
                break;
            }
            case "Right":{
                playerImg = new Image("Sprites/playerRight.png");
                break;
            }
            case "Left":{
                playerImg = new Image("Sprites/playerLeft.png");
                break;
            }
            case "Bottom":{
                playerImg = new Image("Sprites/playerDown.png");
                break;
            }
        }
        gc.setFill(new ImagePattern(playerImg));
        gc.fillOval(
                model.getPlayer().getX(),
                model.getPlayer().getY(),
                model.getPlayer().getW(),
                model.getPlayer().getH()
        );

        //Bullets/Bombs
        Image bombImage = new Image("Sprites/bombe.png");
        for (Bullet bullet: this.model.getBullets()){
            gc.setFill(new ImagePattern(bombImage));
            gc.fillOval(
                    bullet.getX(),
                    bullet.getY(),
                    bullet.getW(),
                    bullet.getH()
                    );
        }

        //Zombies
        Image zombieImg = new Image("Sprites/zombieRight.png");
        for (Zombie zb:this.model.getZombies()){
            String zombieDirect = zb.getZombieDirection();
            switch (zombieDirect) {
                case "Top": {
                    zombieImg = new Image("Sprites/zombieTop.png");
                    break;
                }
                case "Right": {
                    zombieImg = new Image("Sprites/zombieRight.png");
                    break;
                }
                case "Left": {
                    zombieImg = new Image("Sprites/zombieLeft.png");
                    break;
                }
                case "Bottom": {
                    zombieImg = new Image("Sprites/zombieDown.png");
                    break;
                }
            }
            gc.setFill(new ImagePattern(zombieImg));
            gc.fillOval(
                    zb.getX() ,
                    zb.getY() ,
                    zb.getW(),
                    zb.getH()
            );
        }

        gc.setFill(Color.BLACK);
        gc.setFont(new Font(24));
        gc.fillText("Score: "+ model.getScore()/1000 +" seconds",750,575);
    }
}
