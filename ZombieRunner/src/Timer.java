import Models.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private long previousTime = -1;
    private Model model;
    private Graphics graphics;

    private long elapsedTime;

    public Timer(Model model, Graphics graphics){
        this.model = model;
        this.graphics = graphics;
    }

    public void handle(long nowNano){
        long nowMilli = nowNano / 1000000;
        if(previousTime == -1){
            elapsedTime = 0;
        }else {
            elapsedTime = nowMilli - previousTime;
        }

        previousTime = nowMilli;
        model.update(elapsedTime);
        graphics.draw();
        model.timeInSecs(elapsedTime);
    }
}
