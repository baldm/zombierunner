import Models.Bullet;
import Models.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {
    private Model model;

    public InputHandler(Model model){
        this.model = model;
    }

    public void onKey(KeyCode key){
        // prepare walk
         setPlayerWalkSettings(key);
         // walk diag
         setPlayerDiagonalWalk();
         // walk straight
         setPlayerStraightWalk(key);
         // plant Bomb
         if(key == KeyCode.ENTER && model.getBulletCount()!=0){
             Bullet bullet = new Bullet(model.getPlayer().getX(),model.getPlayer().getY());
             model.addBullets(bullet);
             model.setBulletCount(model.getBulletCount()-1);
         }
    }

    public void setPlayerWalkSettings(KeyCode key){
        if(key == KeyCode.W){
            model.getPlayer().setDirection("Top");
            model.getPlayer().setTop(true);
        }
        if(key == KeyCode.S){
            model.getPlayer().setDirection("Bottom");
            model.getPlayer().setBottom(true);
        }
        if(key == KeyCode.A){
            model.getPlayer().setDirection("Left");
            model.getPlayer().setLeft(true);
        }
        if(key == KeyCode.D){
            model.getPlayer().setDirection("Right");
            model.getPlayer().setRight(true);
        }
    }

    public void setPlayerDiagonalWalk(){
        int diagSpeed = 6;
        if(model.getPlayer().isTop() && model.getPlayer().isRight()){
            model.getPlayer().move(diagSpeed,-diagSpeed);
        }
        if(model.getPlayer().isTop() && model.getPlayer().isLeft()){
            model.getPlayer().move(-diagSpeed,-diagSpeed);
        }
        if(model.getPlayer().isBottom() && model.getPlayer().isRight()){
            model.getPlayer().move(diagSpeed,diagSpeed);
        }
        if(model.getPlayer().isBottom() && model.getPlayer().isLeft()){
            model.getPlayer().move(-diagSpeed,diagSpeed);
        }
    }

    public void setPlayerStraightWalk(KeyCode key){
        if(key == KeyCode.W){
            model.getPlayer().setDirection("Top");
            model.getPlayer().setTop(true);
             model.getPlayer().move(0, -10);
        }
        if(key == KeyCode.S){
            model.getPlayer().setDirection("Bottom");
            model.getPlayer().setBottom(true);
             model.getPlayer().move(0,10);
        }
        if(key == KeyCode.A){
            model.getPlayer().setDirection("Left");
            model.getPlayer().setLeft(true);
            model.getPlayer().move(-10,0);
        }
        if(key == KeyCode.D){
            model.getPlayer().setDirection("Right");
            model.getPlayer().setRight(true);
             model.getPlayer().move(10,0);
        }
    }

    public void onKeyRelease(KeyCode key){
        if(key == KeyCode.W){
            model.getPlayer().setDirection("");
            model.getPlayer().setLastState("top");
            model.getPlayer().setTop(false);
        }
        if(key == KeyCode.S){
            model.getPlayer().setDirection("");
            model.getPlayer().setLastState("right");
            model.getPlayer().setBottom(false);
        }
        if(key == KeyCode.A){
            model.getPlayer().setDirection("");
            model.getPlayer().setLastState("left");
            model.getPlayer().setLeft(false);
        }
        if(key == KeyCode.D){
            model.getPlayer().setDirection("");
            model.getPlayer().setLastState("bottom");
            model.getPlayer().setRight(false);
        }
    }
}
