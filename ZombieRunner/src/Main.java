import Models.Model;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class Main extends Application {


    // Eigenschaften initialisieren
    private Timer timer;
    MediaPlayer mediaPlayer;

    // Konstruktoren

    // Methoden
    @Override
    public void start(Stage stage) throws Exception {
        // Music
//        String musicFile = "Media/pokemon.wav";
//        Media sound = new Media(new File(musicFile).toURI().toString());
//        mediaPlayer = new MediaPlayer(sound);
//        mediaPlayer.play();


        /* javaFX Vorbereitungen / Stage
        /////////////////////////////////*/

        // Canvas
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

        // Group
        Group root = new Group();
        root.getChildren().add(canvas);
        // Label


        //Scene
        Scene scene = new Scene(root);

        scene.setFill(Color.LIGHTGREEN);
        // Stage
        stage.setScene(scene);
        stage.show();

        // Draw
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model,gc);
        timer = new Timer(model, graphics);
        timer.start();

//         InputHandler
        InputHandler inputHandler = new InputHandler(model);
         scene.setOnKeyPressed(
                 event -> inputHandler.onKey(event.getCode())
         );
         scene.setOnKeyReleased(
                 event -> inputHandler.onKeyRelease(event.getCode())
         );
    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }

}
